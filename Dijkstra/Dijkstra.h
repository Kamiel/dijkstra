//
//  Dijkstra.h
//  Dijkstra
//
//  Created by DAXX on 7/26/16.
//  Copyright © 2016 DAXX. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol Comparable <NSObject>

- (NSComparisonResult)compare:(id <Comparable> _Nonnull)instance;

@end


@class Edge;


// This types (Node, Edge) isn't suitable for pure calculations, but effective and simple
// Otherwise, should use table with numbers here, not pointers.


@interface Node : NSObject <NSCopying, Comparable>

@property (nonatomic, strong, nonnull) NSArray <Edge *>                     *edges;
@property (nonatomic, assign)          NSUInteger                            path;

@end


@interface Edge : NSObject <NSCopying, Comparable>

@property (nonatomic, strong, nonnull) Node                                 *node;
@property (nonatomic, assign)          NSUInteger                            value;

+ (instancetype _Nullable)edgeToNode:(Node *_Nonnull)node
                           withValue:(NSUInteger)value;

@end


@class RACSignal;


@interface Dijkstra : NSObject

/** @brief all nodes here */
@property (nonatomic, strong, nonnull)   NSMutableArray <Node *>              *graph;
/** @brief each element of route contain shortest previous element for shortest path */
@property (nonatomic, strong, nonnull)   NSDictionary <Node *, Node *> *route; // !!!: NSMutableDictionary is not KVO-compatible,
// see: https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/KeyValueObserving/Articles/KVOCompliance.html#//apple_ref/doc/uid/20002178-BAJEAIEE
/** @brief just show shortest path vector generated from <tt>route</tt>.
    <tt>RaCSequence *</tt> contain <tt>NSArray <NSNumber *> *</tt>.
 */
@property (nonatomic, strong, readonly, nonnull)  RACSequence *shortestPath;
- (instancetype _Nullable)initWithGraph:(NSArray <Node *> *_Nonnull)nodes;
/** @brief do Dijkstra shortest path calculations */
- (RACSignal *_Nonnull)calculation;

@end

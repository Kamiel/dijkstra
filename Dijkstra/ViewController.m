//
//  ViewController.m
//  Dijkstra
//
//  Created by DAXX on 7/26/16.
//  Copyright © 2016 DAXX. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa.h>
#import <ReactiveCocoa/RACEXTScope.h>
#import "Dijkstra.h"
#import "ViewController.h"


@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIButton   *calculate;
@property (weak, nonatomic) IBOutlet UILabel    *label;
@property (weak, nonatomic) IBOutlet UITextView *result;
@property (weak, nonatomic) IBOutlet UILabel    *message;

@property (strong, nonatomic)        Dijkstra   *model;

@end


@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.model = [self createModel];

    RAC(self.result, text) = [[RACObserve(self.model, shortestPath) map:^id(RACSequence *_Nonnull path) {
        return [path foldLeftWithStart:@""
                                reduce:^NSString *_Nonnull (NSString *_Nonnull accumulator,
                                                            NSNumber *_Nonnull value) {
                                    return [accumulator stringByAppendingFormat:@" %@",
                                            value.stringValue];
                                }];
    }] deliverOnMainThread];

    @weakify(self)

    self.calculate.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self)

        return [self.model calculation];
    }];

    RAC(self.calculate, hidden)  = self.calculate.rac_command.executing;
    RAC(self.message, hidden)    = [self.calculate.rac_command.executing not];
}

#pragma mark -

// No UI to input because of time limitation

- (Dijkstra *_Nonnull)createModel
{
    // no smart graph constructor because of time limitation
    Node *node1 = Node.new;
    Node *node2 = Node.new;
    Node *node3 = Node.new;
    Node *node4 = Node.new;
    Node *node5 = Node.new;

    // see: https://habrastorage.org/getpro/habr/post_images/693/49d/f50/69349df50d9ca60c1fff348e9b0b40ad.jpg

    node1.edges = @[[Edge edgeToNode:node2 withValue:10],
                    [Edge edgeToNode:node3 withValue:30],
                    [Edge edgeToNode:node4 withValue:50],
                    [Edge edgeToNode:node5 withValue:10]
                    ];
    node2.edges = @[];
    node3.edges = @[[Edge edgeToNode:node5 withValue:10]
                    ];
    node4.edges = @[[Edge edgeToNode:node2 withValue:40],
                    [Edge edgeToNode:node3 withValue:20]
                    ];
    node5.edges = @[[Edge edgeToNode:node1 withValue:10],
                    [Edge edgeToNode:node3 withValue:10],
                    [Edge edgeToNode:node4 withValue:30]
                    ];
    Dijkstra *model = [[Dijkstra alloc] initWithGraph:@[node1,
                                                        node2,
                                                        node3,
                                                        node4,
                                                        node5]];
    return model;
}

@end

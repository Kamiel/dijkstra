//
//  Dijkstra.m
//  Dijkstra
//
//  Created by DAXX on 7/26/16.
//  Copyright © 2016 DAXX. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa.h>
#import <ReactiveCocoa/RACEXTScope.h>
// !!!: in product code private functionality should be duplicated in own sources
#import <ReactiveCocoa/RACDynamicSequence.h>
#import "Dijkstra.h"


@interface Dijkstra ()

@property (nonatomic, strong, nonnull) NSMutableSet *visited;

/** @brief use initialize before each <tt>- calculate</tt> call */
- (RACSequence *_Nonnull)initialize;
/** @brief do Dijkstra shortest path calculations, do <tt>- initialize</tt> required first */
- (RACSequence *_Nonnull)calculate;

@end


#pragma mark -


@interface RACSequence (Comparator)

/** @brief check order before each <tt>map</tt> */
- (instancetype)order:(NSComparisonResult (^)(_Nonnull id, _Nonnull id))cmptr
                  map:(id _Nonnull (^)(id _Nonnull value))block;
/** @brief like usual <tt>- [NSArray sortedArrayUsingComparator:]</tt>, but for RACStream */
- (instancetype)sort:(NSComparisonResult (^)(_Nonnull id, _Nonnull id))cmptr;

@end


#pragma mark -


@implementation Node

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone
{
    self.edges = [self.edges copyWithZone:zone];
    return self;
}

#pragma mark - Comparable

- (NSComparisonResult)compare:(Node *_Nonnull)instance
{
    return [@(self.path) compare:@(instance.path)];
}

@end


@implementation Edge

+ (instancetype _Nullable)edgeToNode:(Node *_Nonnull)node
                           withValue:(NSUInteger)value
{
    return [[Edge alloc] initWithNode:node andValue:value];
}

- (instancetype)initWithNode:(Node *_Nonnull)node
                    andValue:(NSUInteger)value
{
    self = [super init];

    if (self) {
        self.node  = node;
        self.value = value;
    }
    return self;
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone
{
    self.node = [self.node copyWithZone:zone];
    return self;
}

#pragma mark - Comparable

- (NSComparisonResult)compare:(Edge *_Nonnull)instance
{
    return [@(self.value) compare:@(instance.value)];
}

@end


#pragma mark -


@implementation Dijkstra

#pragma mark - public

- (instancetype)initWithGraph:(NSArray <Node *> *_Nonnull)nodes
{
    self = [super init];

    if (self) {
        self.graph   = [nodes mutableCopy];
        self.route   = NSDictionary.new;
        self.visited = [NSMutableSet setWithCapacity:self.graph.count];

        @weakify(self)

        RAC(self, shortestPath) = [RACObserve(self, route) map:
                                   ^RACSequence *(NSMutableDictionary <Node *, Node *> *_Nonnull route) {

                                       return [self.graph.rac_sequence map:^NSNumber *_Nonnull(Node *node) {
                                           Node *_Nullable nearest = route[node];

                                           @strongify(self)

                                           return nearest ? ({
                                               Node *_Nonnull nearest_ = nearest;
                                               @([self.graph indexOfObject:nearest_] + 1ull);
                                           }) : @0;
                                       }];
                                   }];
    }
    return self;
}

- (RACSignal *_Nonnull)calculation
{
    return [[self initialize] concat:[self calculate]].signal;
}

/*

#pragma mark - Imperative style

- (NSArray <Edge *> *_Nonnull)sortedEdges_:(NSArray <Edge *> *_Nonnull)edges {
    return
    [edges sortedArrayUsingComparator:
     ^NSComparisonResult(Edge *_Nonnull edge1,
                         Edge *_Nonnull edge2) {
         return [edge1 compare:edge2];
     }];
}

- (void)calculate_ {
    Node *node;

    while ((node = [self getMinimal_])) {
        
        for (Edge *edge in [self sortedEdges_:node.edges]) {
            if ([self.visited containsObject:edge.node]) {
                continue;
            }
            NSUInteger newPath = node.path + edge.value;
            
            if (newPath < edge.node.path) {
                edge.node.path = newPath;
                self.route[edge.node] = node;
            }
        }
        [self.visited addObject:node];
    }
}

// not effective implementation:
// here check all the nodes to find
// one with minimal path value.
// To improve: use sorting tree here.
- (Node *_Nullable)getMinimal_ {
    NSUInteger minPath = INFINITY;
    Node *minNode = nil;

    for (Node *node in self.graph) {
        if ([self.visited containsObject:node]) {
            continue;
        }
        if (node.path < minPath) {
            minNode = node;
            minPath = node.path;
        }
    }
    return minNode;
}

- (void)initialize_ {
    [self.visited removeAllObjects];

    for (Node *node in self.graph) {
        node.path = INFINITY;
    }
    if (self.graph.count > 0) {
        self.graph.firstObject.path = 0;
    }
}

*/

#pragma mark - FRP style

- (RACSequence *)sortedEdges:(NSArray <Edge *> *_Nonnull)edges {
    return
    [edges.rac_sequence sort:
     ^NSComparisonResult(Edge *_Nonnull edge1,
                         Edge *_Nonnull edge2) {
         return [edge1 compare:edge2];
     }];
}

- (RACSequence *)calculate
{
    @weakify(self)

    return
    [self.graph.rac_sequence order:^NSComparisonResult(Node *_Nonnull node1,
                                                       Node *_Nonnull node2) {
        return [node1 compare:node2];
    }
                               map:^Node  *_Nonnull(Node *_Nonnull node) {
                                   node.edges = [self calculateEdges:node].array; // doh, yep, I use mutable states
                                   // because `- order:map:` will check `[self.visited containsObject:edge.node]`
                                   // in `- calculateEdges:` on next map iteration.

                                   @strongify(self)

                                   [self.visited addObject:node];

                                   return node;
                               }];
}

- (RACSequence *)calculateEdges:(Node *_Nonnull)node
{
    @weakify(self)

    return
    [[[self sortedEdges:node.edges] filter:^BOOL(Edge *edge) {
        @strongify(self)

        return ! [self.visited containsObject:edge.node];
    }] map:^Edge *(Edge *edge) {
        @strongify(self)

        NSUInteger newPath = node.path + edge.value;
        NSLog(@"Edge from %@ to %@",
              @([self.graph indexOfObject:node] + 1ull),
              @([self.graph indexOfObject:edge.node] + 1ull));
        NSLog(@"<<<\rnode path: %@\redgevalue: %@\redge node path: %@",
              @(node.path), @(edge.value), @(edge.node.path));

        if (newPath < edge.node.path) { // Dijstra algorithm logic
            edge.node.path = newPath;

            NSMutableDictionary <Node *, Node *> *_Nonnull route_ = [self.route mutableCopy];
            route_[edge.node] = node; // side effect here
            // but return value is still pure
            self.route = [route_ copy]; // to be sure that observation called
            NSLog(@"ROUTE: %@", [self.shortestPath.array description]);
        }
        return edge;
    }];
}

// lazy version
- (RACSequence *)initialize
{
    RACSequence *initTail = [[self.graph subarrayWithRange:NSMakeRange(1ull, self.graph.count - 1ull)].rac_sequence
                             map:^Node *(Node *node) {
                                 node.path = INFINITY; // ???: for infinity (use kCFNumberPositiveInfinity instead?)
                                 return node;
                             }];

    @weakify(self)

    return
    [RACDynamicSequence sequenceWithLazyDependency:^id{
        @strongify(self)
        [self.visited removeAllObjects]; // side effect here

        return nil;
    } headBlock:^id(id _) {
        return ^ {
            @strongify(self)
            Node *_Nullable node = self.graph.rac_sequence.head;
            node.path = 0;
        };
    } tailBlock:^RACSequence *(id _) {
        return initTail;
    }];
}

@end


#pragma mark -


@implementation RACSequence (Comparator)

- (instancetype)order:(NSComparisonResult (^)(_Nonnull id, _Nonnull id))cmptr
                  map:(id _Nonnull (^)(id _Nonnull value))block
{
    NSCParameterAssert(cmptr != nil);
    NSCParameterAssert(block != nil);

    // see: http://stackoverflow.com/a/19905407
    __block __weak RACSequence *(^makeTailRecursion_)(RACSequence *_Nonnull seq);
    RACSequence *(^makeTail)(RACSequence *_Nonnull seq);

    // leak here...
    makeTail = ^RACSequence *(RACSequence *_Nonnull seq){
        if (! seq) { // force break out recursion
            return [RACSequence empty];
        }
        RACSequence *sorted = [seq sort:cmptr];
        
        id _Nullable head = sorted.head; // greedy here
        RACSequence *tail = sorted.tail;

        __block RACSequence *(^makeTailRecursion)(RACSequence *_Nonnull seq) = makeTailRecursion_;

        // yeeeah, recursion here!
        return
        [RACSequence sequenceWithHeadBlock:^id{
            return head ? ({ id _Nonnull head_ = head; block(head_); }) : nil;
        } tailBlock:^RACSequence *{
            return makeTailRecursion(tail); // <- right here
        }];
    };
    makeTailRecursion_ = makeTail;

    return makeTail(self);
}

// eager calculations here yet
- (instancetype)sort:(NSComparisonResult (^)(_Nonnull id, _Nonnull id))cmptr
{
    RACSequence *seq = [self.array sortedArrayUsingComparator:cmptr].rac_sequence;
    // TODO: invistigate how to wrap it in lazy container.

    return seq;
}

@end

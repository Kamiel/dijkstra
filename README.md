
In my Dijkstra algorithm implementation there a lot RAC overusage, just for example. See commented imperative implementation as well (just for example, and it is not effective). FRP implementation not pure yet, it use mutable states somewhere because of function returning types simplicity (think about instance methods like about functions in State Monad, where instance variables contains state).

***

According to `ReactiveCocoa` current state, from [ReactiveCocoa README](https://github.com/ReactiveCocoa/ReactiveCocoa#objective-c-and-swift):

> The Objective-C API will continue to exist and be supported for the foreseeable future, but it won’t receive many improvements. For more information about using this API, please consult our legacy documentation.
>
> We highly recommend that all new projects use the Swift API.

*:(*

